#include "command_handler.h"

using namespace std;

vector<string> split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (ss >> part)
        commands.push_back(part);
    
    return commands;
}

int detect_and_run(vector<string> command){
    if(command.size() == 0)
        return -1;

    if (command[0] == EXIT){
        if(command.size() != 1)
            return -1;
        exit_program();
    }   
    if (command[0] == NEW_CLIENT){
        if(command.size() != 3)
            return -1;
        create_new_client(command[1], command[2]);
    }
    if (command[0] == NEW_ROUTER){
        if(command.size() != 2)
            return -1;
        create_new_router(stoi(command[1]));
    }
    if (command[0] == CONNECT){
        if(command.size() != 4)
            return -1;
        connect_client_to_router(command[1], stoi(command[2]), stoi(command[3]));
    }
    if (command[0] == GET_GROUP_LIST){
        if(command.size() != 1)
            return -1;
        get_group_list();
    }
    if (command[0] == JOIN_GROUP){
        if(command.size() != 3)
            return -1;
        join_group(command[1], command[2]);
    }
    if (command[0] == LEAVE_GROUP){
        if(command.size() != 3)
            return -1;
        leave_group(command[1], command[2]);
    }
    if (command[0] == SEND_MESSAGE){
        if(command.size() != 4)
            return -1;
        send_message(command[1], command[2], command[3]);
    }
    if (command[0] == SEND_FILE){
        if(command.size() != 4)
            return -1;
        send_file(command[1], command[2], command[3]);
    }
    if (command[0] == SHOW_GROUP){
        if(command.size() != 2)
            return -1;
        show_groups(command[1]);
    }
    if (command[0] == SIGN_OUT){
        if(command.size() != 2)
            return -1;
        sign_out(command[1]);
    }
    if (command[0] == CONNECT_ROUTER){
        if(command.size() != 5)
            return -1;
        connect_router_to_router(stoi(command[1]), stoi(command[2]), stoi(command[3]), stoi(command[4]));
    }
    if (command[0] == DISCONNECT_ROUTER){
        if(command.size() != 3)
            return -1;
        disconnect_router(stoi(command[1]), stoi(command[2]));
    }
    if (command[0] == SHOW_TABLE){
        if(command.size() != 2)
            return -1;
        show_table(stoi(command[1]) );
    }
    return 0;
}