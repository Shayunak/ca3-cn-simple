#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <algorithm>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <algorithm>

//client commands
#define NEW_CLIENT "new_client"
#define CONNECT "connect"
#define GET_GROUP_LIST "get_group_list"
#define SEND_MESSAGE "send_message"
#define SEND_FILE "send_file"
#define CONNECT_ROUTER "connect_router"
#define JOIN_GROUP "join"
#define LEAVE_GROUP "leave"
#define SHOW_GROUP "show_group"
#define SIGN_OUT "sign_out"
// router commands
#define NEW_ROUTER "new_router"
#define CONNECT_ROUTER "connect_router"
#define DISCONNECT_ROUTER "disconnect_router"
#define SHOW_TABLE "show"
//
#define EXIT "exit"

#define CLIENT_FILE "client.out"
#define ROUTER_FILE "router.out"
#define DELIMITER '#'

#define PIPE_WRITE_END 1
#define PIPE_READ_END 0
#define NO_PIPE -1
#define MAX_MESSAGE_LENGTH 1600
#define MAX_DATA_LENGTH 1500
#define MAX_FD_SELECT 23
//connect:c
//send:s
//join:j
//leave:l
//disconnect: dc
//sh: show
//so: sign_out
//format to client:c/router_named_pipe_read/router_named_pipe_write/router_id
//format to router:c/client_named_pipe_read/client_named_pipe_write/client_ip/port_number
//format to router:dc/router_id
//format to client:so
//format to client:sf/file_name/group
//format to client:sm/body/group
//format to router:cr/router_named_pipe_read/router_named_pipe_write/router_id/port_number
//format to client:j/group
//format to client:l/group
//format to client:sh
//format to router:sh

//IGMP frame format: mr/group
//                   lr/group
//DVMRP frame format: dv/group
// prune              pr/group
//frame file format: multi_file/group/id/data
//frame message formar: multi_message/group/data
//frame client signout to router: so ->
//    router to client Ack: ok <-
#endif