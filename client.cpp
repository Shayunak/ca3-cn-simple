#include "config.h"
using namespace std;

int client_number;
string client_directory_path;
int parent_pipe_fd;
int router_pipe_receive_fd = NO_PIPE;
int router_pipe_send_fd = NO_PIPE;
string router_pipe_receive_name = "";
string router_pipe_send_name = "";
fd_set pipe_set;
string client_file_log;
int last_sent_file;
vector<string> joined_groups;

string make_client_directory(){
    string directory_path = "./Client" + to_string(client_number);
    mkdir(&directory_path[0], 0777);
    string revised_directory_path = directory_path + "/";
    return revised_directory_path;  
}

void write_log(string message){
    fstream log_file;
    log_file.open(client_directory_path + client_file_log, fstream::app);
    log_file << message << endl;
    log_file.close();
}

void reset_set(){
    FD_ZERO(&pipe_set);
    FD_SET(parent_pipe_fd, &pipe_set);
    if (router_pipe_receive_fd != NO_PIPE)
        FD_SET(router_pipe_receive_fd, &pipe_set);
}

vector<string> split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (getline(ss, part, DELIMITER) )
        commands.push_back(part);
    
    return commands;
}

void create_and_send_frame(string group_name,char* data){
    string frame;
    frame = "multi_file#" + group_name + DELIMITER + group_name +  "_" + to_string(last_sent_file) + DELIMITER + data;
    write(router_pipe_send_fd, &frame[0], frame.length());
    write_log("Sent a frame to group " + group_name);
}

bool check_connection(){
    if (router_pipe_receive_fd != NO_PIPE && router_pipe_send_fd != NO_PIPE)
        return true;
    return false;
}

bool check_group_exists(string group_name){
    for (unsigned int i = 0; i < joined_groups.size(); i++){
        if (joined_groups[i] == group_name)
            return true;
    }
    return false;
}

void send_file(string file_name, string group_name){
    // check connection
    if (!check_connection()){
        write_log("clinet signed out and can't send anything!");
        return;
    }
    //check group exist
    if (!check_group_exists(group_name)){
        write_log("client has not joined to this group!");
        return;
    }
    // reading from file to a buffer
    string content;
    ifstream resource_file(file_name);
    resource_file.seekg(0, ios::end);
    size_t length = resource_file.tellg();
    resource_file.seekg(0, ios::beg);
    content.assign((istreambuf_iterator<char>(resource_file)),(istreambuf_iterator<char>()));
    resource_file.close();

    int remainder = length % MAX_DATA_LENGTH;
    int packet_num = (int) (length / MAX_DATA_LENGTH);
    for (int i = 0; i < packet_num; i++){
        create_and_send_frame(group_name, &content.substr(i * MAX_DATA_LENGTH, MAX_DATA_LENGTH)[0]);
        sleep(0.01);
    }
    if (remainder != 0)
        create_and_send_frame(group_name, &content.substr(packet_num * MAX_DATA_LENGTH, remainder)[0]);

    last_sent_file++;
}

void connect_to_router(string router_named_receive_pipe, string router_named_send_pipe, string router_id){
    router_pipe_receive_name = router_named_receive_pipe;
    router_pipe_send_name = router_named_send_pipe;
    router_pipe_receive_fd = open(&router_named_receive_pipe[0], O_RDWR);
    router_pipe_send_fd = open(&router_named_send_pipe[0], O_RDWR);
    write_log("client Connected to router " + router_id);
}

void signout(){
    if (!check_connection()){
        write_log("clinet signed out and can't send anything!");
        return;
    }
    char buff[MAX_MESSAGE_LENGTH];
    string frame;
    frame = "so";
    write(router_pipe_send_fd, &frame[0], frame.length());
    read(router_pipe_receive_fd, buff, MAX_MESSAGE_LENGTH);
    //closing
    router_pipe_receive_fd = NO_PIPE;
    router_pipe_send_fd = NO_PIPE;
    close(router_pipe_receive_fd);
    close(router_pipe_send_fd);
    unlink(&router_pipe_receive_name[0]);
    unlink(&router_pipe_send_name[0]);
    write_log("client signed out!");
}

void send_message(string body, string group_name){
    // check connection
    if (!check_connection()){
        write_log("clinet signed out and can't send anything!");
        return;
    }
    //check group exist
    if (!check_group_exists(group_name)){
        write_log("client has not joined to this group!");
        return;
    }
    // send message
    string frame;
    frame = "multi_message#" + group_name + DELIMITER + body;
    write(router_pipe_send_fd, &frame[0], frame.length());
    write_log("Sent a message to group " + group_name);
}

void join_group(string group_name){
    // check connection
    if (!check_connection()){
        write_log("clinet signed out and can't send anything!");
        return;
    }
    //check group exist
    if (check_group_exists(group_name)){
        write_log("client has already joined to this group!");
        return;
    }
    // add to joined groups
    joined_groups.push_back(group_name);
    // send MR message
    string frame;
    frame = "mr#" + group_name;
    write(router_pipe_send_fd, &frame[0], frame.length());
    write_log("client joined to group: " + group_name);
}

void leave_group(string group_name){
    // check connection
    if (!check_connection()){
        write_log("clinet signed out and can't send anything!");
        return;
    }
    //check group exist
    if (!check_group_exists(group_name)){
        write_log("client is not a member of this group!");
        return;
    }
    // remove from joined groups
	joined_groups.erase(remove(joined_groups.begin(), joined_groups.end(), group_name), joined_groups.end());
    // send LR message
    string frame;
    frame = "lr#" + group_name;
    write(router_pipe_send_fd, &frame[0], frame.length());
    write_log("client left the group: " + group_name);
}

void show_groups(){
    write_log("joined groups:");
    for (unsigned int i = 0; i < joined_groups.size(); i++)
        write_log(joined_groups[i]);
}

void detect_and_run(string command){
    vector<string> parsed_command = split_command(command);
    if(parsed_command[0] == "c")
        connect_to_router(parsed_command[1], parsed_command[2], parsed_command[3]);
    else if (parsed_command[0] == "so")
        signout();
    else if(parsed_command[0] == "sf")
        send_file(parsed_command[1], parsed_command[2]);
    else if (parsed_command[0] == "sm")
        send_message(parsed_command[1], parsed_command[2]);
    else if (parsed_command[0] == "j")
        join_group(parsed_command[1]);
    else if (parsed_command[0] == "l")
        leave_group(parsed_command[1]);
    else if (parsed_command[0] == "sh")
        show_groups();
}

void write_to_file(string frame){
    vector<string> parsed_frame = split_command(frame);
    write_log("client Recieved Frame From group " + parsed_frame[1] + " With ID " + parsed_frame[2]);
    string file_name = client_directory_path + "client" + to_string(client_number) + "_" + parsed_frame[2] + ".rec";
    ofstream dest_file;
    dest_file.open(file_name, ios::app);
    dest_file << parsed_frame[3];
    dest_file.close();
}

void detect_incoming_frame(string frame){
    vector<string> parsed_frame = split_command(frame);
    if (parsed_frame[0] == "multi_file")
        write_to_file(frame);
    else if (parsed_frame[0] == "multi_message")
        write_log("message in group " + parsed_frame[1] + " : " + parsed_frame[2]);
}

int main(int argc, char* argv[]){
    client_number = stoi(argv[1]);
    parent_pipe_fd = stoi(argv[2]);
    last_sent_file = 0;
    client_directory_path = make_client_directory();
    client_file_log = "Client" + string(argv[1]) +".log";
    // Close Unnecessary STDIN,STDOUT,STDERR
    close(0);
    close(1);
    close(2);

    char parent_command[MAX_MESSAGE_LENGTH];
    char incoming_frame[MAX_MESSAGE_LENGTH];
    memset(parent_command, 0, MAX_MESSAGE_LENGTH);
    memset(incoming_frame, 0, MAX_MESSAGE_LENGTH);

    write_log("client: " + to_string(client_number) + " created!");

    while (true)
    {
        reset_set();
        if(select(MAX_FD_SELECT, &pipe_set, NULL, NULL, NULL) != 0){
            if (FD_ISSET(parent_pipe_fd, &pipe_set)){
                if (read(parent_pipe_fd, parent_command, MAX_MESSAGE_LENGTH) <= 0)
                    exit(EXIT_FAILURE);
                // write_log(parent_command);
                detect_and_run(string(parent_command));
            }
            if(router_pipe_receive_fd != NO_PIPE && FD_ISSET(router_pipe_receive_fd, &pipe_set) ){
                // write_log("got here____1");
                read(router_pipe_receive_fd, incoming_frame, MAX_MESSAGE_LENGTH);
                detect_incoming_frame(incoming_frame);
            }
        }
        memset(parent_command, 0, MAX_MESSAGE_LENGTH);
        memset(incoming_frame, 0, MAX_MESSAGE_LENGTH);
    }
}