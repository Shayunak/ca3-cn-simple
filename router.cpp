#include "config.h"
using namespace std;

int router_id;
string router_directory_path;
int parent_pipe_fd;
int ports_pipes_fd_iterator = 0;
int ports_pipes_fd[MAX_FD_SELECT] = { NO_PIPE };
// group_name, list_interface
map<string, vector<int>> Forwarding_table;
// port, <receive_fd, send_fd>
map<int, pair<int, int>> Ports_fd;
// router_id, port
map<int, int> Routers;
// client_ip, port
map<string, int> Clients;
fd_set pipe_set;
string router_file_log;
// port, list_group_name
map<int, vector<string>> flood_received;

int find_port_from_receive_fd(int receive_fd){
    for(auto connection = Ports_fd.begin() ; connection != Ports_fd.end(); connection++){
        if((*connection).second.first == receive_fd)
            return (*connection).first;
    }
    return -1;
}

string find_client_from_port(int port){
    for(auto client = Clients.begin() ; client != Clients.end(); client++){
        if((*client).second == port)
            return (*client).first;
    }
    return "NOT";
}

void write_log(string message){
    fstream log_file;
    log_file.open(router_directory_path + router_file_log, fstream::app);
    log_file << message << endl;
    log_file.close();
}

bool is_port_occupied(int port_number){
    for(auto connection = Ports_fd.begin() ; connection != Ports_fd.end(); connection++){
        if((*connection).first == port_number)
            return true;
    }
    return false;
}

string make_router_directory(){
    string directory_path = "./Router" + to_string(router_id);
    mkdir(&directory_path[0], 0777);
    string revised_directory_path = directory_path + "/";
    return revised_directory_path;  
}

void reset_set(){
    FD_ZERO(&pipe_set);
    FD_SET(parent_pipe_fd, &pipe_set);
    for(int i = 0; i < ports_pipes_fd_iterator; i++){
        if(ports_pipes_fd[i] != NO_PIPE)
            FD_SET(ports_pipes_fd[i], &pipe_set);
    }       
}

void add_to_fdset(int receive_fd){
    ports_pipes_fd[ports_pipes_fd_iterator] = receive_fd;
    ports_pipes_fd_iterator++;  
}

void remove_from_fdset(int receive_fd){
    for(int i = 0; i < ports_pipes_fd_iterator; i++){
        if(ports_pipes_fd[i] == receive_fd){
            ports_pipes_fd[i] = NO_PIPE;
            return;
        }
    }    
}

vector<string> split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (getline(ss, part, DELIMITER) )
        commands.push_back(part);
    
    return commands;
}

void connect_to_client(string client_named_receive_pipe, string client_named_send_pipe, string client_ip, int port_number){
    if(is_port_occupied(port_number) ){
        write_log("This port(" + to_string(port_number) + ") is occupied to connect to a client.");
        return;
    }
    int receive_fd = open(&client_named_receive_pipe[0], O_RDWR);
    int send_fd = open(&client_named_send_pipe[0], O_RDWR);
    write_log("Successfuly connected to client " + client_ip + " on port " + to_string(port_number));
    add_to_fdset(receive_fd);
    Ports_fd.insert(pair<int, pair<int, int>>(port_number, pair<int, int>(receive_fd, send_fd)) );
    Clients.insert(pair<string, int>(client_ip, port_number));
}

void connect_to_router(string router_named_receive_pipe, string router_named_send_pipe, int router_id, int port_number){
    if(is_port_occupied(port_number) ){
        write_log("This port(" + to_string(port_number) + ") is occupied to connect to a router.");
        return;
    }
    int receive_fd = open(&router_named_receive_pipe[0], O_RDWR);
    int send_fd = open(&router_named_send_pipe[0], O_RDWR);
    write_log("Successfuly connected to router " + to_string(router_id) + " on port " + to_string(port_number));
    add_to_fdset(receive_fd);
    Ports_fd.insert(pair<int, pair<int, int>>(port_number, pair<int, int>(receive_fd, send_fd)) );
    Routers.insert(pair<int, int>(router_id, port_number));
}

void show_table(){
    stringstream ss;
    ss << "Group \t Interfaces" << endl;
    for(auto entry = Forwarding_table.begin(); entry != Forwarding_table.end(); ++entry){
        ss << (*entry).first << " \t ";
        for(unsigned int i = 0; i < (*entry).second.size(); ++i){
            ss << (*entry).second[i];
            if(i < ((*entry).second.size() - 1))
                ss << ",";
        }
        ss << endl;
    }
    write_log(ss.str());
} 

void disconnect_router(int router_id){
    if(Routers.find(router_id) == Routers.end()){
        write_log("You are not connected to router " + to_string(router_id) + " ;so,you cannot disconnect from it!");
        return;
    }
    int port = Routers[router_id];
    int receive_fd = Ports_fd[port].first;
    int send_fd = Ports_fd[port].second;
    remove_from_fdset(receive_fd);
    close(receive_fd);
    close(send_fd);
    Ports_fd.erase(Ports_fd.find(port));
    Routers.erase(Routers.find(router_id));
    write_log("Successfully disconneted from " + to_string(router_id) + "!");
}

void detect_and_run_command(string command){
    write_log("test2:" + command);
    vector<string> parsed_command = split_command(command);
    if(parsed_command[0] == "sh")
        show_table();
    else if(parsed_command[0] == "c")
        connect_to_client(parsed_command[1], parsed_command[2], parsed_command[3], stoi(parsed_command[4]));
    else if(parsed_command[0] == "cr")
        connect_to_router(parsed_command[1], parsed_command[2], stoi(parsed_command[3]), stoi(parsed_command[4]));
    else if(parsed_command[0] == "dc")
        disconnect_router(stoi(parsed_command[1]));
}

void sign_out(int receive_fd){
    string ack = "ok";
    int port = find_port_from_receive_fd(receive_fd);
    int send_fd = Ports_fd[port].second;
    string client_ip = find_client_from_port(port);
    write(send_fd, &ack[0], ack.size());
    remove_from_fdset(receive_fd);
    close(receive_fd);
    close(send_fd);
    Ports_fd.erase(Ports_fd.find(port));
    Clients.erase(Clients.find(client_ip));
    write_log("Successfully disconnected from client with ip " + client_ip +"!");
}

void broadcast_packet_to_specified_ports(string packet, vector<int> ports){
    for(unsigned int i = 0; i < ports.size(); ++i)
        write(Ports_fd[ports[i]].second, &packet[0], packet.size());
}

void broadcast_flooding_packet(string group_name){
    vector<int> all_routers_ports;
    string flooding_packet = "dv#" + group_name;
    for(auto router_connection = Routers.begin(); router_connection != Routers.end(); ++router_connection)
        all_routers_ports.push_back((*router_connection).second);
    
    broadcast_packet_to_specified_ports(flooding_packet, all_routers_ports);
}

void add_routers_ports_to_group_entry(string group_name){
    if(Forwarding_table.find(group_name) == Forwarding_table.end())
        Forwarding_table.insert(pair<string, vector<int>>(group_name, vector<int>()));
    for(auto router_connection = Routers.begin(); router_connection != Routers.end(); ++router_connection){
        if (find(Forwarding_table[group_name].begin(), Forwarding_table[group_name].end(), (*router_connection).second) == Forwarding_table[group_name].end())
            Forwarding_table[group_name].push_back((*router_connection).second);
    }
}

void join_group(int receive_fd, string group_name){
    int port = find_port_from_receive_fd(receive_fd);
    string client_ip = find_client_from_port(port);
    add_routers_ports_to_group_entry(group_name);
    Forwarding_table[group_name].push_back(port);
    write_log("client with ip " + client_ip + " joined to group \"" + group_name + "\"");
    broadcast_flooding_packet(group_name);
}

void leave_group(int receive_fd, string group_name){
    int port = find_port_from_receive_fd(receive_fd);
    string client_ip = find_client_from_port(port);
    Forwarding_table[group_name].erase(
        remove(Forwarding_table[group_name].begin(), Forwarding_table[group_name].end(), port), 
        Forwarding_table[group_name].end()
    );
    if(Forwarding_table[group_name].size() == 0)
        Forwarding_table.erase(Forwarding_table.find(group_name));
    write_log("client with ip " + client_ip + " left the group \"" + group_name + "\"");
}

void send_prune_packet(int port, string group_name){
    string prune_packet = "pr#" + group_name;
    write(Ports_fd[port].second, &prune_packet[0], prune_packet.size());
 }

void manage_flooding_packet(int receive_fd, string group_name){
    int port = find_port_from_receive_fd(receive_fd);
    write_log("Got a Flooding packet(dv) from " + to_string(port) +"!");
    
    vector<int> routers_ports;
    add_routers_ports_to_group_entry(group_name);
    for(auto router_connection = Routers.begin(); router_connection != Routers.end(); ++router_connection){
        if ((*router_connection).second != port)
            routers_ports.push_back((*router_connection).second);
    }
    if (routers_ports.size() != 0){
        string flooding_packet = "dv#" + group_name;
        broadcast_packet_to_specified_ports(flooding_packet, routers_ports);
    } else{
        if (Forwarding_table[group_name].size() == 1){
            send_prune_packet(port, group_name);
            Forwarding_table.erase(Forwarding_table.find(group_name));
        }
    }
}

void manage_pruning_packet(int receive_fd, string group_name){
    int port = find_port_from_receive_fd(receive_fd);
    write_log("Got a Pruning packet(pr) from " + to_string(port) +"!");
    Forwarding_table[group_name].erase(
        remove(Forwarding_table[group_name].begin(), Forwarding_table[group_name].end(), port), 
        Forwarding_table[group_name].end()
    );
    if (Forwarding_table[group_name].size() == 1 && (find_client_from_port(Forwarding_table[group_name][0]) == "NOT")){
        send_prune_packet(Forwarding_table[group_name][0], group_name);
        Forwarding_table.erase(Forwarding_table.find(group_name));
    }
}

void route_data_packet(int receive_fd, string group_name, string packet){
    int incoming_port = find_port_from_receive_fd(receive_fd);
    if(Forwarding_table.find(group_name) == Forwarding_table.end()){
        write_log("Shit!Group name "+ group_name +" dose not exists!");
        return;
    }
    vector<int> forwarding_ports = Forwarding_table[group_name];
    for(unsigned int i = 0; i < forwarding_ports.size(); ++i){
        if(forwarding_ports[i] != incoming_port && (Ports_fd.find(forwarding_ports[i]) != Ports_fd.end()))
            write(Ports_fd[forwarding_ports[i]].second, &packet[0], packet.size());
    }
    write_log("Routed packet from group_name " + group_name + " on port " + to_string(incoming_port) +"!");
}

void manage_frame(string frame, int receive_fd){
    vector<string> parsed_frame = split_command(frame);
    if(parsed_frame[0] == "so")
        sign_out(receive_fd);
    else if(parsed_frame[0] == "mr")
        join_group(receive_fd, parsed_frame[1]);
    else if(parsed_frame[0] == "lr")
        leave_group(receive_fd, parsed_frame[1]);
    else if(parsed_frame[0] == "dv")
        manage_flooding_packet(receive_fd, parsed_frame[1]);
    else if(parsed_frame[0] == "pr")
        manage_pruning_packet(receive_fd, parsed_frame[1]);
    else    
        route_data_packet(receive_fd, parsed_frame[1], frame);
}

int main(int argc, char* argv[]){
    router_id = stoi(argv[1]);
    parent_pipe_fd = stoi(argv[2]);
    router_directory_path = make_router_directory();
    router_file_log = "Router" + string(argv[1]) +".log";
    // Close Unnecessary STDIN,STDOUT,STDERR
    close(0);
    close(1);
    close(2);
    char parent_command[MAX_MESSAGE_LENGTH];
    char incoming_frame[MAX_MESSAGE_LENGTH];
    memset(parent_command, 0, MAX_MESSAGE_LENGTH);
    memset(incoming_frame, 0, MAX_MESSAGE_LENGTH);

    write_log("router: " + to_string(router_id) + " created!");

    while (true)
    {
        reset_set();
        if(select(MAX_FD_SELECT, &pipe_set, NULL, NULL, NULL) != 0){
            if (FD_ISSET(parent_pipe_fd, &pipe_set)){
                if (read(parent_pipe_fd, parent_command, MAX_MESSAGE_LENGTH) <= 0)
                    exit(EXIT_FAILURE);
                detect_and_run_command(string(parent_command));
            }
            
            for(int i = 0; i < ports_pipes_fd_iterator; i++){
                if(ports_pipes_fd[i] != NO_PIPE && FD_ISSET(ports_pipes_fd[i], &pipe_set)){
                    read(ports_pipes_fd[i], incoming_frame, MAX_MESSAGE_LENGTH);
                    manage_frame(incoming_frame, ports_pipes_fd[i]);
                    memset(incoming_frame, 0, MAX_MESSAGE_LENGTH);
                }
            }
        }
        memset(parent_command, 0, MAX_MESSAGE_LENGTH);
        memset(incoming_frame, 0, MAX_MESSAGE_LENGTH);
    }
}