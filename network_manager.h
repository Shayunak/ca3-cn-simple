#include <string>
#include <unistd.h>
#ifndef NETWORK_H
#define NETWORK_H

struct node_connection{
    pid_t pid;
    int pipe_fd;
};

// clients
void create_new_client(std::string name, std::string ip);
void connect_client_to_router(std::string client_ip, int router_id, int port_number);
void send_file(std::string client_ip, std::string file_name, std::string group_name);
void send_message(std::string client_ip, std::string message_body, std::string group_name);
void get_group_list();
void join_group(std::string client_ip, std::string group_name);
void leave_group(std::string client_ip, std::string group_name);
void show_groups(std::string client_ip);
void sign_out(std::string client_ip);
// routers
void create_new_router(int router_id);
void connect_router_to_router(int router_id_1, int router_id_2, int port_number_1, int port_number_2);
void disconnect_router(int router_id_1, int router_id_2);
void show_table(int router_id);
//
void exit_program();
#endif