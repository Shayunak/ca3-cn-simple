CC := g++ -std=c++11 -pedantic -Wall
all: manager.out client.out router.out

manager.out: manager.o command.o config.h
	$(CC) manager.o command.o -o manager.out

manager.o: network_manager.cpp command_handler.h network_manager.h
	$(CC) -c network_manager.cpp -o manager.o

command.o: command_handler.cpp command_handler.h
	$(CC) -c command_handler.cpp -o command.o

client.out: client.cpp
	$(CC) client.cpp -o client.out

router.out: router.cpp
	$(CC) router.cpp -o router.out

.PHONY: clean
clean:
	rm *.o
	rm manager.out client.out router.out
	rm -rf pipes
	rm -rf Router* Client*

.PHONY: clear
clear:
	rm -rf pipes
	rm -rf Router* Client*