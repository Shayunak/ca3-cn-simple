#include "command_handler.h"
#include "network_manager.h"
using namespace std;

map<int, node_connection> Routers;
map<string, node_connection> Clients;
vector<pair<string, int>> group_list;

int check_group_existence(string group){
    for (unsigned int i = 0; i < group_list.size(); ++i){
        if(group_list[i].first == group)
            return i;
    }
    return -1;
}

bool check_client_existece(string client_ip){
    if(Clients.find(client_ip) != Clients.end() )
        return true;
    return false;
}

bool check_router_existece(int router_number){
    if(Routers.find(router_number) != Routers.end() )
        return true;
    return false;
}

void create_new_client(string name, string ip)
{
    pid_t pid;
    int fdPipe[2];
    if(check_client_existece(ip) ){
        cout << "client with this ip already exists!" << endl;
        return;
    }
    // Making unnamed pipe to the child
    if(pipe(fdPipe) < 0){
        cout << "Could not make unnamed pipes!!" << endl;
        exit(EXIT_FAILURE);
    }
    // forking a new child
    pid = fork();
    if(pid < 0){
        cout << "Could not fork!!" << endl;
        exit(EXIT_FAILURE);
    }else if(pid == 0){
        close(fdPipe[PIPE_WRITE_END]);
        string read_side_pipe = to_string(fdPipe[PIPE_READ_END]);
        char* args[4] = {(char*)CLIENT_FILE, &ip[0], &read_side_pipe[0], NULL}; 
        execv(CLIENT_FILE , args);
    }else{
        close(fdPipe[PIPE_READ_END]);
        node_connection new_client;
        new_client.pipe_fd = fdPipe[PIPE_WRITE_END];    new_client.pid = pid;
        Clients.insert(pair<string, node_connection>(ip, new_client) );
    }
}

void create_new_router(int router_id){
    pid_t pid;
    int fdPipe[2];
    if(check_router_existece(router_id) ){
        cout << "router with this router number already exists!" << endl;
        return;
    }
    // Making unnamed pipe to the child
    if(pipe(fdPipe) < 0){
        cout << "Could not make unnamed pipes!!" << endl;
        exit(EXIT_FAILURE);
    }
    // forking a new child
    pid = fork();
    if(pid < 0){
        cout << "Could not fork!!" << endl;
        exit(EXIT_FAILURE);
    }else if(pid == 0){
        close(fdPipe[PIPE_WRITE_END]);
        string read_side_pipe = to_string(fdPipe[PIPE_READ_END]);
        string router_number_str = to_string(router_id);
        char* args[4] = {(char*)ROUTER_FILE, &router_number_str[0], &read_side_pipe[0], NULL}; 
        execv(ROUTER_FILE , args);
    }else{
        close(fdPipe[PIPE_READ_END]);
        node_connection new_router;
        new_router.pipe_fd = fdPipe[PIPE_WRITE_END];    new_router.pid = pid;
        Routers.insert(pair<int, node_connection>(router_id, new_router) );
    }
}

void connect_client_to_router(string client_ip, int router_id, int port_number)
{
    node_connection client_connection = Clients[client_ip];
    node_connection router_connection = Routers[router_id];
    string first_fifo_name = "./pipes/client" + client_ip + "_" + "router" + to_string(router_id) + "_0.pipe";
    string second_fifo_name = "./pipes/client" + client_ip + "_" + "router" + to_string(router_id) + "_1.pipe";
    if((mkfifo(&first_fifo_name[0] , 0666) < 0) || (mkfifo(&second_fifo_name[0] , 0666) < 0)){
        cout << "Could not make named pipe!!" << endl;
        exit(EXIT_FAILURE);
    }
    string command_to_client = "c#" + first_fifo_name + DELIMITER + second_fifo_name + DELIMITER + to_string(router_id);
    string command_to_router =  "c#" + second_fifo_name + DELIMITER + first_fifo_name + DELIMITER + client_ip + DELIMITER + to_string(port_number);
    write(client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
    write(router_connection.pipe_fd, &command_to_router[0], MAX_MESSAGE_LENGTH);
}

void connect_router_to_router(int router_id_1, int router_id_2, int port_number_1, int port_number_2)
{
    if(router_id_1 == router_id_2){
        cout << "router cannot be connected to itself!" << endl;
        return; 
    }
    node_connection first_router_connection = Routers[router_id_1];
    node_connection second_router_connection = Routers[router_id_2];
    string first_fifo_name = "./pipes/router" + to_string(router_id_1) + "_" + "router" + to_string(router_id_2) + "_0.pipe";
    string second_fifo_name = "./pipes/router" + to_string(router_id_1) + "_" + "router" + to_string(router_id_2) + "_1.pipe";
    if((mkfifo(&first_fifo_name[0] , 0666) < 0) || (mkfifo(&second_fifo_name[0] , 0666) < 0)){
        cout << "Could not make named pipe!!" << endl;
        exit(EXIT_FAILURE);
    }
    string command_to_first_router = "cr#" + first_fifo_name + DELIMITER + second_fifo_name + DELIMITER + to_string(router_id_2) + DELIMITER + to_string(port_number_1);
    string command_to_second_router =  "cr#" + second_fifo_name + DELIMITER + first_fifo_name + DELIMITER + to_string(router_id_1) + DELIMITER + to_string(port_number_2);
    write(first_router_connection.pipe_fd, &command_to_first_router[0], MAX_MESSAGE_LENGTH);
    write(second_router_connection.pipe_fd, &command_to_second_router[0], MAX_MESSAGE_LENGTH);
}

void send_file(string client_ip, string file_name, string group_name){
    node_connection src_client_connection = Clients[client_ip];
    string command_to_client = "sf#" + file_name + DELIMITER + group_name;
    write(src_client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void send_message(string client_ip, string message_body, string group_name){
    node_connection src_client_connection = Clients[client_ip];
    string command_to_client = "sm#" + message_body + DELIMITER + group_name;
    write(src_client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void get_group_list(){
    for (unsigned int i = 0; i < group_list.size(); ++i)
        cout << group_list[i].first << endl;
}

void join_group(string client_ip, string group_name){
    int group_index = check_group_existence(group_name);
    if(group_index < 0)
        group_list.push_back(pair<string, int>(group_name, 1));
    else
        group_list[group_index].second++;
    node_connection client_connection = Clients[client_ip];
    string command_to_client = "j#" + group_name;
    write(client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void leave_group(string client_ip, string group_name){
    int group_index = check_group_existence(group_name);
    if(group_index < 0){
        cout << "This group does not exist!" << endl; 
        return;
    }else{
        group_list[group_index].second--;
        if(group_list[group_index].second == 0)
            group_list.erase(group_list.begin() + group_index);
    }
    node_connection client_connection = Clients[client_ip];
    string command_to_client = "l#" + group_name;
    write(client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void show_groups(string client_ip){
    node_connection client_connection = Clients[client_ip];
    string command_to_client = "sh";
    write(client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void sign_out(string client_ip){
    node_connection client_connection = Clients[client_ip];
    string command_to_client = "so";
    write(client_connection.pipe_fd, &command_to_client[0], MAX_MESSAGE_LENGTH);
}

void disconnect_router(int router_id_1, int router_id_2){
    if(router_id_1 == router_id_2){
        cout << "router cannot be disconnected from itself!" << endl;
        return; 
    }
    node_connection first_router_connection = Routers[router_id_1];
    node_connection second_router_connection = Routers[router_id_2];
    string command_to_first_router = "dc#" + to_string(router_id_2);
    string command_to_second_router =  "dc#" + to_string(router_id_1);
    write(first_router_connection.pipe_fd, &command_to_first_router[0], MAX_MESSAGE_LENGTH);
    write(second_router_connection.pipe_fd, &command_to_second_router[0], MAX_MESSAGE_LENGTH);
}

void show_table(int router_id){
    node_connection router_connection = Routers[router_id];
    string command_to_router = "sh";
    write(router_connection.pipe_fd, &command_to_router[0], MAX_MESSAGE_LENGTH);
}

void exit_program(){
    for(auto router_connection = Routers.begin(); router_connection != Routers.end(); router_connection++){
        close((*router_connection).second.pipe_fd);
        kill((*router_connection).second.pid, SIGKILL);
    }
    for(auto client_connection = Clients.begin(); client_connection != Clients.end(); client_connection++){
        close((*client_connection).second.pipe_fd);
        kill((*client_connection).second.pid, SIGKILL);
    }
    exit(EXIT_SUCCESS);
}

int main(){
    if(mkdir("./pipes", 0777) < 0){
        cout << "Could not create the pipes's folder." << endl;
        exit(EXIT_FAILURE);
    }
    string line;
    while (getline(cin, line)){
        if(detect_and_run(split_command(line)) < 0)
            cout << "Your command is in a wrong format!" << endl;
    }
    return 0;
}