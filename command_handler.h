#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H
#include "network_manager.h"
#include "config.h"

std::vector<std::string> split_command(std::string command);
int detect_and_run(std::vector<std::string>);

#endif